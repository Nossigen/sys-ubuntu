# sys-ubuntu

[[_TOC_]]

--- 

- Prerequisites : Ubutu 24.04 LTS
- This installation script :
```bash
sudo apt install gnome-tweaks gnome-tweak-tool gnome-extensions-app
sudo apt install ubuntu-unity-backgrounds
```

## Update system

### Update apt sources

```bash
sudo apt update; sudo apt upgrade -y
```

### Update snap sources

- List snap sources that can be updated
```bash
snap refresh --list
```

- Update each one individualy
```bash
snap refresh <app>
```

## Gnome setup

- [settings app](./docs/app/settings.md)
- [tweaks app](./docs/app/tweaks.md)
- [extensions app](./docs/app/extensions.md)

## Term setup


## Dev setup


