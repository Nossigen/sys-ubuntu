# Settings

## Power

- Power saving > Automatic suspend > On Battery > Delay > 1 hour
- General > Show Battery Percentage > True

## Multitasking 

- Multi-monitor > Workspaces on all displays
- workspace > Fixed Workspace size > 6

---

- App Switching > Include apps from the current workspace only

## Appearance

- Color > Purple
- Style > Dark

---

- Picture > Purple crown

## Unity Desktop

- Desktop icons > Size > Small
- Desktop icons > Position of new icons > Bottom left
- Desktop icons > Show Home Folder > false

---

- Dock > Auto-hide > true
- Dock > Icon size > 36
- Dock > Show on > All displays

---

- Enhanced tilling > Disable

## Search

- Search Location > Defaults Location > Downloads > false
- Search Location > Defaults Location > Music > false
- Search Location > Defaults Location > Pictures > false
- Search Location > Defaults Location > Videos > false 
- Search Location > Custom Location > Add ~/Space folder  

---

- Search results > Calendar > False
- Search results > Characters > False
- Search results > Clocks > False
- Search results > Password and keys > False

## Mouse and touchpad

- Mouse > Mouse acceleration > False

## Keyboard

- Input Sources > English (US) + English (US, alt intl.)

---

- Input Source Switching > Switch input sources individualy for each winows

---

- Special character entry > Alternate Characters keys > None

### Keyboard shortcuts

- Accessibility > Disable all
- Launcher > Disable all
- Navigation :
    - Hide all normal windows : Disable
    - Switch to workspace left : Meta + h
    - Switch to workspace right : Meta + l 
- Screenshot :
    - Disable all except next one
    - Take a screenshot interactively : Meta + Shift + s
- Sound and Media : Nothing to change
- System :
    - Show notification list : Meta + N
    - Lock screen : Meta + <ESC>
    - Disable everything else
- Windows :
    - Close window : Meta + Q
    - Disable everything else


> TO Finish

