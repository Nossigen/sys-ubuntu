# Extensions

## Ubuntu dock settings

Panel mode > False
Screen position > Bottom

---

- Appearance > Shrink the dash > False
- Appearance > Use built-in theme > false
- Appearance > Customize windows counter indicators > Settings > Use dominant colors > true
- Appearance > Customize dash color > true > Darkest purple
- Appearance > Customize opacity > Fixed > 20%


##  [Gnome-shell-data-format](https://github.com/KEIII/gnome-shell-panel-date-format?tab=readme-ov-file)

```bash
dconf write /org/gnome/shell/extensions/panel-date-format/format "'%a %d %b(%m) %H:%I (w%V)'"
```